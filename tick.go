package main

import (
	"os"
	"time"

	"github.com/faiface/beep"
	"github.com/faiface/beep/speaker"
	"github.com/faiface/beep/wav"
)

type metronome struct {
	File       string
	BPM        int
	Offset     int // ms
	SampleRate beep.SampleRate
}

func (m metronome) startMetronome(stop chan struct{}) error {
	tock, err := os.Open(m.File)
	if err != nil {
		return err
	}

	tockUnsampled, tockFmt, err := wav.Decode(tock)
	if err != nil {
		return err
	}

	tockStr := beep.Resample(1, // quality, 1 for low latency low quality
		tockFmt.SampleRate,
		m.SampleRate,
		tockUnsampled,
	)

	tockBuf := beep.NewBuffer(tockFmt)
	tockBuf.Append(tockStr)
	tockUnsampled.Close()

	tick := time.NewTicker(
		time.Minute / time.Duration(m.BPM),
	)

	time.Sleep(time.Duration(m.Offset) * time.Millisecond)

	for {
		select {
		case <-stop:
			return nil
		case <-tick.C:
			tock := tockBuf.Streamer(0, tockBuf.Len())
			speaker.Play(tock)
		}
	}
}
