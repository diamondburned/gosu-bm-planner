package assets

import "strings"

var (
	// Break contains an empty break
	Break = objectBreak{
		object{Fill: ' '},
	}
)

// DrawBreak draws empty spaces for breaks
func DrawBreak(length uint) string {
	var b strings.Builder

	for i := 0; i < int(length); i++ {
		b.WriteRune(Break.Fill)
	}

	return b.String()
}
