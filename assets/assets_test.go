package assets

import "testing"

func BenchmarkJoinObjects(b *testing.B) {
	const (
		size      = 4  // default recommended
		sliderLen = 16 // case
		height    = 4
	)

	for n := 0; n < b.N; n++ {
		var (
			objects = []string{
				DrawCircle(size),
				DrawCircle(size),
				DrawCircle(size),
				DrawSlider(size, sliderLen, 0),
				DrawCircle(size),
				DrawBreak(size * 1), // default 2 spaces applies, 2 break len = 1 circ break
				DrawCircle(size),
				DrawCircle(size),
				DrawCircle(size),
				DrawSlider(size, sliderLen, 1),
				DrawBreak(size * 1),
				DrawCircle(size),
				DrawCircle(size),
				DrawCircle(size),
				DrawSlider(size, sliderLen, 1),
				DrawCircle(size),
				DrawCircle(size),
				DrawCircle(size),
				DrawSlider(size, sliderLen, 1),
				DrawSlider(size, sliderLen, 0),
				DrawCircle(size),
				DrawCircle(size),
				DrawCircle(size),
				DrawCircle(size),
				DrawCircle(size),
				DrawCircle(size),
				DrawBreak(size * 1),
			}
		)

		println(JoinObjects(objects, height, 2))
	}
}
