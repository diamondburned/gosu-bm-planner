package assets

import "strings"

var (
	// Circle contains the rune '█' for a circle
	Circle = objectCircle{filledobject}
)

// DrawCircle generates a one line circle
func DrawCircle(size uint) string {
	var b strings.Builder

	for i := 0; i < int(size); i++ {
		b.WriteRune(Circle.Fill)
	}

	return b.String()
}
