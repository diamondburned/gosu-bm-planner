package assets

import "strings"

type object struct {
	Fill rune
}

type objectCircle struct {
	object
}

type objectSlider struct {
	object
	Left  rune
	Right rune
}

type objectBreak struct {
	object
}

var (
	filledobject = object{'█'}
)

// JoinObjects takes in a list of one-line objects and render them
func JoinObjects(os []string, height, spacing uint) string {
	var b strings.Builder

	for i, o := range os {
		b.WriteString(o)

		if i < len(os)-1 {
			for i := 0; i < int(spacing); i++ {
				b.WriteByte(' ')
			}
		}
	}

	var (
		s = b.String()
		a = make([]string, height)
	)

	for i := 0; i < int(height); i++ {
		a[i] = s
	}

	return strings.Join(a, "\n")
}

func max(i, j uint) uint {
	if i > j {
		return i
	}

	return j
}
