package assets

import "strings"

var (
	// Slider contains all the runes for a slider
	Slider = objectSlider{
		object: object{'▒'},
		Left:   '█',
		Right:  '█',
	}
)

// DrawSlider generates a one line slider
// size controls the thickness of the LR borders
// length controls the length
func DrawSlider(size, length, repeat uint) string {
	var (
		b strings.Builder
	)

	repeat++

	for i := 0; i < int(size); i++ {
		b.WriteRune(Slider.Left)
	}

	for r := uint(0); r < repeat; r++ {
		for i := 0; i < int(length)-int(size*2); i++ {
			b.WriteRune(Slider.Fill)
		}

		for i := 0; i < int(size); i++ {
			b.WriteRune(Slider.Right)
		}
	}

	return b.String()
}
