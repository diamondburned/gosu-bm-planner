module gitlab.com/diamondburned/gosu-bm-planner

go 1.12

require (
	github.com/faiface/beep v1.0.1
	github.com/gizak/termui/v3 v3.0.0
	github.com/maruel/panicparse v1.1.1 // indirect
)
