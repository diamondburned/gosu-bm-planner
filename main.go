package main

import (
	"errors"
	"flag"
	"os"
	"time"

	"github.com/faiface/beep"
	"github.com/faiface/beep/mp3"
	"github.com/faiface/beep/speaker"

	ui "github.com/gizak/termui/v3"
)

const metronomeName = "tick.wav"

var (
	errNoFilename = errors.New("No filename given for $1")
	errBPMnotset  = errors.New("BPM not set! Set with -b flag")
)

func main() {
	if err := ui.Init(); err != nil {
		panic(err)
	}

	defer ui.Close()

	var (
		beatMinute int
		tickOffset int
	)

	flag.IntVar(&tickOffset, "t", 0, "The tick offset for metronome (ms), optional")
	flag.IntVar(&beatMinute, "b", 0, "Beat per minute")

	flag.Parse()

	if beatMinute == 0 {
		panic(errBPMnotset)
	}

	var filename string

	if len(flag.Args()) == 1 {
		filename = flag.Args()[0]
	}

	if filename == "" {
		panic(errNoFilename)
	}

	song, err := os.Open(filename)
	if err != nil {
		panic(err)
	}

	songStr, songFmt, err := mp3.Decode(song)
	if err != nil {
		panic(err)
	}

	defer songStr.Close()

	if err := speaker.Init(
		songFmt.SampleRate,
		songFmt.SampleRate.N(time.Second/30),
	); err != nil {
		panic(err)
	}

	stopMetronome := make(chan struct{})

	met := metronome{
		File:       metronomeName,
		BPM:        beatMinute,
		Offset:     tickOffset,
		SampleRate: songFmt.SampleRate,
	}

	go func() {
		done := make(chan struct{})
		speaker.Play(beep.Seq(songStr, beep.Callback(func() {
			done <- struct{}{}
		})))

		<-done
		stopMetronome <- struct{}{}
	}()

	done := make(chan struct{})
	go func() {
		if err := met.startMetronome(stopMetronome); err != nil {
			panic(err)
		}

		done <- struct{}{}
	}()

	<-done
}
