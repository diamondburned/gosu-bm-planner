# gosu-bm-planner 

BeatmapID: 267699

`gr -b 176 -t 1363 klk.mp3`

## Todo for the small assets

- [ ] Make an estimation function for the total length of line
- [ ] Make a custom view to print them
- [ ] Find a way to move the objects to the left at a constant speed
- [ ] Match the speed to the proper BPM\*AR values

## Roadmap

1. [x] Song playback
2. [x] Metronome and offset
3. [ ] Status UI
4. [ ] Play/Pause through UI
5. [ ] Progress bar in UI
6. [ ] Registering and displaying keypresses
7. [ ] Snapping keypresses to the BPM
8. [ ] Record the keypresses
9. [ ] Algorithms to convert keypresses to 1/4 sliders and circles
10. [ ] Display them properly in the UI

